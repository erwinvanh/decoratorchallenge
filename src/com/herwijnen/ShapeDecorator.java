package com.herwijnen;

public abstract class ShapeDecorator implements IShape {
    protected String lineStyle;
    protected IShape iShape;

    public ShapeDecorator(IShape iShape) {
        this.iShape = iShape;
        this.lineStyle = "solid";
    }

    public void setLineStyle(String lineStyle) {
        this.lineStyle = lineStyle;
    }

    @Override
    public void draw() {
        iShape.draw();
    }
}
