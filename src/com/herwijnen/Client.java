package com.herwijnen;

public class Client {

    public static void main(String[] args) {
        System.out.println("*** Decorator Challenge ***");
        IShape circle = new Circle();
        circle.draw();
        System.out.println("*******");

        ShapeDecorator red = new RedShapeDecorator(circle);
        red.draw();

        System.out.println("*******");
        IShape rectangle = new Rectangle();
        ShapeDecorator green = new GreenShapeDecorator(rectangle);
        green.setLineStyle("dotted");
        green.draw();


    }
}
