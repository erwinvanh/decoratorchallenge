package com.herwijnen;

public class GreenShapeDecorator extends ShapeDecorator{

    public GreenShapeDecorator(IShape iShape) {
        super(iShape);
        lineStyle = "solid";
    }

    @Override
    public void draw() {
        super.draw();
        System.out.println("Border color : Green");
        System.out.println("Line-Style : " + lineStyle);
    }
}
