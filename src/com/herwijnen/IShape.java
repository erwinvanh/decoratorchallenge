package com.herwijnen;

public interface IShape {
    void draw();
}
