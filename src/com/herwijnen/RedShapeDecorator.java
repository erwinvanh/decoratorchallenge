package com.herwijnen;

public class RedShapeDecorator extends ShapeDecorator {

    public RedShapeDecorator(IShape iShape) {
        super(iShape);
    }

    @Override
    public void draw() {
        super.draw();
        System.out.println("Border color : red");
        System.out.println("Line-style : " + lineStyle);

    }
}
